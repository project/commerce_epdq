Commerce ePDQ
=============
A module providing a Drupal Commerce payment method for the Barclaycard ePDQ
gateway.


References
----------
- ePDQ CPI Integration Guide:
  http://www.barclaycard.co.uk/business/documents/pdfs/cpi_integration_guidev10.0.pdf


Dependencies
------------
- Commerce (drupal.org/project/commerce), and the submodules:
  * Commerce UI
  * Commerce Payment
  * Commerce Order


Copyright
---------
Copyright 2012 UCLU (University College London Union)
25 Gordon St, London WC1H 0AY
http://uclu.org/


Author
-----
Patrick Dawkins (drupal.org username: pjcdawkins)
